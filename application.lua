print("\nStarting server...");

-- Vars:
-- pins
pGreen = 5; --D5 -- GPIO14
pYellow = 7; --D7 -- GPIO13
pRed = 8; --D8 

-- GPIO
gpio.mode(pGreen, gpio.OUTPUT);
gpio.mode(pYellow, gpio.OUTPUT);
gpio.mode(pRed, gpio.OUTPUT);

function turn_on(pin)
	gpio.write(pin, gpio.HIGH);
end

function turn_off(pin)
	gpio.write(pin, gpio.LOW);
end

turn_off(pGreen)
turn_off(pYellow)
turn_off(pRed)

connected = false;

--MQTT
m = mqtt.Client("nodemcu01", 120, "userMqtt", "senhaMqtt")
--m = mqtt.Client("nodemcu01", 120)
m:on("connect", function(client) print ("mqtt connected") end)
m:on("offline", function(client) print ("mqtt offline") end)

-- on publish message receive event
m:on("message", function(client, topic, data)
  print(topic .. ":" )
  if data ~= nil then
    print(data)
    t = sjson.decode(data)
    print(t.status)
    refresh_lights(t.status) 
  end
end)

-- for TLS: m:connect("192.168.11.118", secure-port, 1)
    m:connect("soldier.cloudmqtt.com", 10231, false, function(client)
        print("connected")
        -- Calling subscribe/publish only makes sense once the connection
        -- was successfully established. You can do that either here in the
        -- 'connect' callback or you need to otherwise make sure the
        -- connection was established (e.g. tracking connection status or in
        -- m:on("connect", function)).

        -- subscribe topic with qos = 0
        client:subscribe("/local", 0, function(client) print("subscribe success") end)
        -- publish a message with data = hello, QoS = 0, retain = 0
        client:publish("/teste", "hello from nodemcu", 0, 0, function(client) print("sent") end)
        connected = true;
    end,
    function(client, reason)
        print("failed reason: " .. reason)
    end)

function refresh_lights(status) 
    turn_off(pYellow);
    turn_off(pRed);
    turn_off(pGreen);
    if (status == 0) then
        turn_on(pRed);
    elseif(status == 1) then
        turn_on(pGreen);
    elseif(status == 2) then
        turn_on(pYellow);
    end
end

--MQTT END
print("Server started!");

